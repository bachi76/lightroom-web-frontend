# Lightroom Catalog - Sample web application #

This is a simple proof-of-concept web application that demonstrates the usage of my [Lightroom catalog access library](https://bitbucket.org/bachi76/lightroomcatalog).

It is built in java with the [Play Framework](https://www.playframework.com/), please refer to the Play website for how to get it up and running.

There's a simple http basic authentication, the (hardcoded) pw is "bachstein". You'd need to plug in your own authentication scheme.

If anybody would like to help making this demo a working product, let me know. 
You can reach me at 'bachi dot insign at gmail dot com'.
