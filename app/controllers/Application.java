package controllers;

import actions.BasicAuth;
import ch.insign.lightroom.LrCatalog;
import ch.insign.lightroom.LrImageAccessor;
import ch.insign.lightroom.model.*;
import play.Logger;
import play.mvc.*;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import com.typesafe.config.ConfigFactory;

@BasicAuth
public class Application extends Controller {

    private static final int DEFAULT_PREV = 4;

    private static String lrdataFolderPath = ConfigFactory.load().getString("lrdataFolderPath");
    private static String imageFolderPath = ConfigFactory.load().getString("imageFolderPath");
    private static String lrcatPath = ConfigFactory.load().getString("lrcatPath");

//    private static String lrdataFolderPath = "/home/bachi/Desktop/Lightroom_5_Catalog_Previews.lrdata";
//    private static String imageFolderPath = "/home/bachi/workspace-play/lightroom/frontend/lr-previews";
//    private static String lrcatPath = "/home/bachi/Desktop/Lightroom_5_Catalog.lrcat";

    private static LrCatalog catalog;

    private static LrCatalog catalog() {
        if (catalog == null) {
            try {
                catalog = new LrCatalog(lrcatPath, lrdataFolderPath, imageFolderPath);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return catalog;
    }


    public static Result index() {
        return ok(views.html.index.render());
    }

    public static Result collections() {

        try {

            catalog().collections();
            return ok(views.html.collections.render(catalog()));


        } catch (SQLException e) {
            e.printStackTrace();
            return internalServerError();
        }
    }

    public static Result folders() {

        try {

            List<LrFolder> folders = catalog().folders().queryBuilder()
                .orderBy("pathFromRoot", true)
                .query();
            return ok(views.html.folders.render(folders));


        } catch (SQLException e) {
            e.printStackTrace();
            return internalServerError();
        }
    }

    public static Result collection(Long id) {

        List<LrImageAccessor> images = new ArrayList<>();

        try {
            LrCollection collection = catalog().collections().queryForId(id);

            for (LrImage lrimg : catalog().images().in(collection)) {
                LrImageAccessor image = catalog().image(lrimg);

                if (image.previews().size() == 0){
                    Logger.warn("No preview found for image id: " + image.id());
                    continue;
                }

                image.setPreviewLevel(DEFAULT_PREV);
                images.add(image);
            }

            return ok(views.html.collection.render(collection.getName(), images));


        } catch (SQLException e) {
            e.printStackTrace();
            return internalServerError(e.getMessage());
        }

    }

    public static Result folder(Long id) {

        List<LrImageAccessor> images = new ArrayList<>();

        try {
            LrFolder folder = catalog().folders().queryForId(id);

            for (LrImage lrimg : catalog().images().in(folder)) {
                LrImageAccessor image = catalog().image(lrimg);

                if (image.previews().size() == 0){
                    Logger.warn("No preview found for image id: " + image.id());
                    continue;
                }
                image.setPreviewLevel(DEFAULT_PREV);
                images.add(image);
            }

            return ok(views.html.folder.render(folder.getPathFromRoot(), images));


        } catch (SQLException e) {
            e.printStackTrace();
            return internalServerError(e.getMessage());
        }

    }

    public static Result image(Long imageId, int previewIndex) {

        try {

            LrImageAccessor image = catalog().image(imageId);

            if (image.file() == null) {
                return notFound();
            }

//            String filename = imageFolderPath + "/"
//                + folder.getPathFromRoot()
//                + lrfile.getBaseName() + "-" + previewIndex + ".jpg";
//
//            File preview = new File(filename);

            Optional<File> prev = image.previewFile(previewIndex);

            // Not found, try to extract
            if (!prev.isPresent()) {
                image.extract();
                prev = image.previewFile(previewIndex);
            }

            if (prev.isPresent()) {
                return ok(prev.get()).as("image/jpeg");
            } else {
                return notFound("Preview not found, extracting..");
            }


        } catch (SQLException e) {
            e.printStackTrace();
            return internalServerError(e.getMessage());
        }




    }


}
