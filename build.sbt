import com.typesafe.sbt.SbtNativePackager._
import NativePackagerKeys._
import com.typesafe.sbt.packager.archetypes.ServerLoader.SystemV

name := """lightroom-web-frontend"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.1"

resolvers += Resolver.mavenLocal

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  javaWs,
  "ch.insign.lightroom" % "lrcat-core" % "1.0-SNAPSHOT" withSources()
)

// include the server settings
packageArchetype.java_server

// global package settings
packageDescription := "Lightroom Catalog Viewer"

// Maintainer for all packages
maintainer := "Martin Bachmann<m.bachmann@insign.ch>"

packageSummary in Linux := "Web frontend for Lighroom catalog and previews."

daemonUser in Linux := "lightroom"         // user which will execute the application

daemonGroup in Linux := (daemonUser in Linux).value // group which will execute the application

serverLoading in Linux := SystemV

// Map the default config files to the universal mappings
mappings in Universal <+= (packageBin in Compile, baseDirectory ) map { (_, base) =>
  val conf = base / "conf" / "application.conf"
  conf -> "conf/application.conf"
}
//mappings in Universal <++= sourceDirectory  map { src =>
  // val resources = src / "main" / "resources"
  // val log4j = resources / "log4j2.xml"
  // val reference = resources / "reference.conf"
  // Seq(log4j -> "conf/log4j2.xml", reference -> "conf/application.conf")
//}
